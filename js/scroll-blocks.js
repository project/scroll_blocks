/**
 * @file
 * Defines JavaScript behaviors for the scroll_blocks module.
 */

(function (Drupal) {

  Drupal.behaviors.scroll_blocks = {
    attach: function (context) {
      var timeout;
      var scroll_blocks = context.querySelectorAll('.scroll-blocks');

      // Listen to scroll event to enable/disable scrollblocks.
      window.addEventListener('scroll', function () {
        // If there's a timer, cancel it.
        if (timeout) {
          window.cancelAnimationFrame(timeout);
        }

        // Call the function in a debounced way.
        timeout = window.requestAnimationFrame(function () {
          scrollBlocksHandler();
        });
      });

      // Loops through all scroll blocks and make it show or hide, depending
      // if it applies or not.
      function scrollBlocksHandler() {
        for (i = 0; i < scroll_blocks.length; ++i) {
          var min_scroll_distance = parseInt(scroll_blocks[i].dataset.scrollblocksMinScrollDistance);
          var max_scroll_distance = parseInt(scroll_blocks[i].dataset.scrollblocksMaxScrollDistance);
          var min_width = parseInt(scroll_blocks[i].dataset.scrollblocksMinWidth);
          var max_width = parseInt(scroll_blocks[i].dataset.scrollblocksMaxWidth);

          var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
          var yPosition = window.pageYOffset;
          var heightOK = yPosition >= min_scroll_distance && yPosition <= max_scroll_distance;

          var minWidthOK = (!isNaN(min_width) && min_width !== 0) ? (windowWidth >= min_width) : true;
          var maxWidthOK = (!isNaN(max_width) && max_width !== 0) ? (windowWidth <= max_width) : true;

          if (heightOK && minWidthOK && maxWidthOK) {
            // Show block.
            showBlock(scroll_blocks[i]);
          }
          else {
            // Hide block.
            hideBlock(scroll_blocks[i]);
          }

        }
      }

      // Show a given block.
      function showBlock(block) {
        if (block.getAttribute('data-scroll-blocks--visible') != 'true') {
          var mutedByUser = block.getAttribute('data-scroll-blocks--disable');
          if (mutedByUser == 'true') {
            console.log(mutedByUser);
            return;
          }

          block.classList.add('scroll-blocks--visible');
          if (block.querySelectorAll('.scroll-blocks__close-button').length == 0) {
            var closeButton = document.createElement('span');
            closeButton.classList.add('scroll-blocks__close-button');
            closeButton.addEventListener('click', function () {
              block.setAttribute('data-scroll-blocks--disable', true);
              block.classList.remove('scroll-blocks--visible');
              block.setAttribute('data-scroll-blocks--visible', false);
            });
            block.appendChild(closeButton);
          }
          block.setAttribute('data-scroll-blocks--visible', true);
        }
      }

      // Hide a given block.
      function hideBlock(block) {
        if (block.getAttribute('data-scroll-blocks--visible')) {
          block.classList.remove('scroll-blocks--visible');
          block.setAttribute('data-scroll-blocks--visible', false);
        }
      }

    }
  };

}(Drupal));
